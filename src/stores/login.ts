import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("counter", () => {
  const username = ref("");
const isLogin = computed(() => {
  //username is not empty
  return username.value !== '';
})

const login = (userName: string): void => {
  username.value = userName;
  localStorage.setItem("username", userName);
};

const logout = () => {
  username.value = "";
  localStorage.removeItem("username");
};

const loadData = () => {
  username.value = localStorage.getItem("username") || "";
}

  return { username, isLogin, login, logout, loadData};
});
